from flask import Flask, request, render_template, redirect, abort, url_for, send_file, flash, jsonify
from natasha import Segmenter, MorphVocab, NewsEmbedding, NewsMorphTagger, NewsSyntaxParser, NewsNERTagger, PER, NamesExtractor, Doc
from werkzeug.utils import secure_filename
from pdf2image import convert_from_path
from PIL import Image
from os.path import basename, isfile, join
import glob
import shutil
import zipfile
import pytesseract
import cv2
import matplotlib.pyplot as plt
import os
import re


app = Flask(__name__)
app.config['UPLOAD_PATH'] = 'uploads'
app.secret_key = 'U3BhY2VfR3VhcmQ'

@app.route("/")
def index():
    return render_template("index.html")

# ? ####### Загрузка файлов ################

@app.route('/', methods=['POST'])
def upload_files():
    uploaded_file = request.files['file']
    filename = secure_filename(uploaded_file.filename)
    filename_wo_ext = filename.split(".")[0]
    filename_format = filename.split(".")[1]
    if 'uploads/'+filename_wo_ext not in glob.glob(os.path.join(app.config['UPLOAD_PATH'])+'/*'):
    # Создаем папки /uploads; /uploads/images_from_file; /uploads/depersonalize_images
    	os.makedirs(os.path.join(app.config['UPLOAD_PATH'])+"/{}".format(filename_wo_ext))
    	os.makedirs(os.path.join(app.config['UPLOAD_PATH'])+"/{}".format(filename_wo_ext)+"/images_from_file")
    	os.makedirs(os.path.join(app.config['UPLOAD_PATH'])+"/{}".format(filename_wo_ext)+"/depersonalize_images")
    	if filename != '':
        # загружаем файл в папку /Uploads
        	uploaded_file.save(os.path.join(app.config['UPLOAD_PATH']+"/{}".format(filename_wo_ext), filename))

    	if filename_format.lower() == 'docx':
        	os.system("abiword --to=pdf {}".format(os.path.join(app.config['UPLOAD_PATH'])+"/{}/{}".format(filename_wo_ext, filename)))
        	os.system("rm {}".format(os.path.join(app.config['UPLOAD_PATH'])+"/{}/{}".format(filename_wo_ext, filename)))
    
    	if filename_format.lower() == 'xlsx':
        	os.system("unoconv -f pdf {}".format(os.path.join(app.config['UPLOAD_PATH'])+"/{}/{}".format(filename_wo_ext, filename)))
        	os.system("rm {}".format(os.path.join(app.config['UPLOAD_PATH'])+"/{}/{}".format(filename_wo_ext, filename)))



    return redirect(url_for('index'))

# ? ####### Скачивание файлов ################

@app.route("/download")
def download(): 
    path = os.path.dirname(os.path.abspath(__file__))+"/uploads/"
    zipf = zipfile.ZipFile('Result.zip','w', zipfile.ZIP_DEFLATED)
    folders = os.listdir(path)
    # Заносим полученные изображения в zip архив
    for folder in folders:
        for file in os.listdir(path+folder+"/depersonalize_images"):
            zipf.write(path+folder+"/depersonalize_images/"+file, "{}\{}".format(folder,file), zipfile.ZIP_DEFLATED )
    zipf.close()

    # Удаляем все папки с документами
    for root, dirs, files in os.walk(app.config['UPLOAD_PATH']):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))

    # Отправляем архим с документами пользователю
    return send_file('Result.zip',
            mimetype = 'zip',
            cache_timeout = -1,
            attachment_filename= 'Result.zip',
            as_attachment = True)

# ? ####### Обезличивание файлов ################

@app.route("/depersonalize", methods=['POST'])
def depersonalize():  
    path = os.path.dirname(os.path.abspath(__file__))+"/uploads/"
    list_folders_files = os.listdir(path)
    count_words = 0
    CheckBox = str(request.form['checkboxvalue'])
    # Проход по всем папкам с документами
    for folder_file in list_folders_files:
        count = 1
        current_file = [f for f in os.listdir(path+folder_file) if isfile(join(path+folder_file, f))][0]

        # Если исходный файл формата .jpg, то просто сохраняем его в папку
        if "jpg" in current_file:
            path_img = path+folder_file+"/{}".format(current_file)
            shutil.copy(path_img, path+folder_file+'/images_from_file/')

        # Если исходный файл формата .pdf, то вытаскиваем из него все страницы в формате .jpg и сохраняем в папку
        elif "pdf" in current_file:
            images = convert_from_path(path+folder_file+"/{}".format(current_file))
            for i in range(len(images)):
                images[i].save(path+folder_file+'/images_from_file/page'+ str(i) +'.jpg', 'JPEG')

        file_images = os.listdir(path+folder_file+"/images_from_file")

        # Проходим по каждой картинке формата .jpg
        for image_file in file_images:
            image = cv2.imread(path+folder_file+"/images_from_file"+"/"+image_file)

            # Вытаскиваем текст + на каких именно пикселях находятся слова
            text = pytesseract.image_to_string(image, lang="rus")
            image_copy = image.copy()
            data = pytesseract.image_to_data(image, output_type=pytesseract.Output.DICT, lang="rus")

            if 'Телефон' in CheckBox:           
                word_occurences = []
                for i, word in enumerate(data["text"]):
                    if word in re.findall(r"(?:(?:8|\+7)[\- ]?)?(?:\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}", word):
                        word_occurences.append(i)

                for occ in word_occurences:
                    count_words += 1
                    # извлекаем ширину, высоту, верхнюю и левую позицию для обнаруженного слова
                    w = data["width"][occ]
                    h = data["height"][occ]
                    l = data["left"][occ]
                    t = data["top"][occ]
                    # определяем все точки окружающей рамки
                    p1 = (l, t)
                    p3 = (l + w, t + h)
                    # рисуем прямоугольник
                    image_copy = cv2.rectangle(image_copy, p1, p3, (255, 0, 0),-1)      

            if 'Дата рождения' in CheckBox:
                numbers = re.findall(r"[0-9]{2}\.[0-9]{2}\.[0-9]{4}",text)
                for number in numbers:
                    word_occurences = []
                    for i, word in enumerate(data["text"]):
                        if word == number:
                            word_occurences.append(i)

                    for occ in word_occurences:
                        count_words += 1
                        # извлекаем ширину, высоту, верхнюю и левую позицию для обнаруженного слова
                        w = data["width"][occ]
                        h = data["height"][occ]
                        l = data["left"][occ]
                        t = data["top"][occ]
                        # определяем все точки окружающей рамки
                        p1 = (l, t)
                        p3 = (l + w, t + h)
                        # рисуем прямоугольник
                        image_copy = cv2.rectangle(image_copy, p1, p3, (255, 0, 0),-1)        


            


            if 'ФИО' in CheckBox:
                # Создаем модель для поиска в тексте ФИО
                segmenter = Segmenter()
                morph_vocab = MorphVocab()
                emb = NewsEmbedding()
                morph_tagger = NewsMorphTagger(emb)
                syntax_parser = NewsSyntaxParser(emb)
                ner_tagger = NewsNERTagger(emb)
                names_extractor = NamesExtractor(morph_vocab)
                
                doc = Doc(text)
                doc.segment(segmenter)
                doc.tag_morph(morph_tagger)
                doc.tag_ner(ner_tagger)
                for token in doc.tokens:
                    token.lemmatize(morph_vocab)
                for span in doc.spans:
                    span.normalize(morph_vocab)
                for span in doc.spans:
                    if span.type == PER:
                        span.extract_fact(names_extractor)

                g = list({_.text for _ in doc.spans})
                k = []
                for i in g:
                    k.append(i.split(' '))

                find_FIO = []
                for i in k:
                    for j in i:
                        find_FIO.append(j)
                
                # создаем список из слов-исключений
                avoid_word = ['моск','департамент', 'распоряжени','правительст','инфо','государст','мэр', 'собянин','бюджет','учрежден','город','центр','контроль','монитор','госинс']

                # Ищем совпадения найденных ФИО со всеми найденными словами и заносим их координаты в массив 
                for word_find in find_FIO:
                    word_occurences = []
                    for i, word in enumerate(data["text"]):
                        k = True
                        for name in avoid_word:
                            if (name in word.lower()) or (name in word_find.lower()):
                                k = False

                        if len(word)>2:
                            if (word == word_find) and (word[0].isupper()) and (k):
                                word_occurences.append(i)

                            if (word[-1]== ',') and (word[:-1] == word_find) and (word[0].isupper()) and (k):
                                    word_occurences.append(i)
                            if (word in word_find.split('\n')) and (word[0].isupper()) and (k):
                                word_occurences.append(i)
                    

                    # Закрашиваем найденные ФИО 
                    for occ in word_occurences:
                        count_words += 1
                        # извлекаем ширину, высоту, верхнюю и левую позицию для обнаруженного слова
                        w = data["width"][occ]
                        h = data["height"][occ]
                        l = data["left"][occ]
                        t = data["top"][occ]
                        # определяем все точки окружающей рамки
                        p1 = (l, t)
                        p3 = (l + w, t + h)
                        # рисуем прямоугольник
                        image_copy = cv2.rectangle(image_copy, p1, p3, (255, 0, 0),-1)

            if 'Почта' in CheckBox:
                text_eng = pytesseract.image_to_string(image)
                data_eng = data = pytesseract.image_to_data(image, output_type=pytesseract.Output.DICT)
                
                mails = re.findall(r'[\w\.-]+@[\w\.-]+', text_eng)
                for mail in mails:
                    word_occurences = []
                    for i, word in enumerate(data_eng["text"]):
                        if word == mail:
                            word_occurences.append(i)

                    for occ in word_occurences:
                        count_words += 1
                        # извлекаем ширину, высоту, верхнюю и левую позицию для обнаруженного слова
                        w = data_eng["width"][occ]
                        h = data_eng["height"][occ]
                        l = data_eng["left"][occ]
                        t = data_eng["top"][occ]
                        # определяем все точки окружающей рамки
                        p1 = (l, t)
                        p3 = (l + w, t + h)
                        # рисуем прямоугольник
                        image_copy = cv2.rectangle(image_copy, p1, p3, (255, 0, 0),-1) 


            count += 1
            plt.imsave(path+folder_file+"/depersonalize_images/"+image_file[:-4]+str(count)+"result.jpg", image_copy)
   

    return jsonify({'success' : "Файл обезличен", "count": "Обезличено слов: "+str(count_words)})



if __name__ == "__main__":
    app.run(host="0.0.0.0")
